from flask import Flask, render_template, url_for, request, redirect, jsonify,Response
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import json, random

app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer , primary_key=True)
    type = db.Column(db.Integer , nullable=False)
    value = db.Column(db.String(200) , nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

def __repr__(self):
    return '<Task %r>'% self.type

@app.route('/event' , methods = ['POST','GET'])
def add_event():
    c_type=request.args.get('type',default='*')
    c_value=request.args.get('value',default='*')
    print(dict(request.form))
    if c_type == '*':
        print(111)
        content_type = request.form['type']
        content_value = request.form['value']
        print(content_type, content_value)
        new_event =  Todo(type=content_type , value = content_value)
    else:
        new_event =  Todo(type=c_type , value = c_value)
    try:
        db.session.add(new_event)
        db.session.commit()
        return render_template('index.html')
    except:
        return 'There was an issue adding your event'
    return render_template('index.html')

# @app.route('/event' , methods = ['POST','GET'])
# def add_event():
#     if request.method == 'POST':
#         content_type=request.args.get('type',default='*')
#         content_value=request.args.get('value',default='*')
#         new_event =  Todo(type=content_type , value = content_value)

#         try:
#             db.session.add(new_event)
#             db.session.commit()
#             return render_template('index.html')
#         except:
#             return 'There was an issue adding your event'
#     return render_template('index.html')

@app.route('/events' , methods = ['GET'])
def all_events():
    events = Todo.query.order_by(Todo.date_created).all()
    #data = {'Type': events.type, 'Value': 'events.value'}
    return render_template('events.html',events=events)
    #return Response(json.dumps(data), mimetype = 'application/json')
    #return jsonify(events).serialize

@app.route('/value' , methods = ['GET'])
def value():
    events = Todo.query.order_by(Todo.date_created).all()
    sum=0
    for e in events:
        if e.type == 'increment':
            sum=sum+int(e.value)
        else:
            sum=sum-int(e.value)
    return str(sum)

@app.route('/value/<int:t>')
def value_t(t):
    t=t
    sum=0
    result=[]
    events = Todo.query.order_by(Todo.date_created).all()
    if t>len(events):
            return redirect('/value')
    else:
        for i in range(0,t):
            result.append(events[i])

        for e in result:
            if e.type == 'increment':
                sum=sum+int(e.value)
            else:
                sum=sum-int(e.value)
        return str(sum)

@app.route('/service')
def random_event():
    item=['increment','decrement']
    value_r=random.randrange(1,5)
    type_temp=random.choices(item)
    if type_temp==0:
        type_r='increment'
    else:
        type_r='decrement'

    new_event =  Todo(type=type_r , value = value_r)

    try:
        db.session.add(new_event)
        db.session.commit()
        return render_template('index.html')
    except:
        return 'There was an issue adding your event'



@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

if __name__ == "__main__":
    app.run(debug=True)